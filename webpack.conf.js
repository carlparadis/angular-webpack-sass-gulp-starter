var WebpackNotifierPlugin = require('webpack-notifier');

module.exports = {
    entry: '',
    output: {
        path: '',
        filename: ''
    },
    module: {
        loaders: [
            {test: /\.html$/, loader: "raw"},
            {test: /\.js$/, loaders: ["ng-annotate", "babel-loader"], exclude: /node_modules|templates.js|_test.js|mock-backend/},
            {test: /\.js$/, loader: "eslint-loader", exclude: /node_modules|bower_components|templates.js|_test.js|mock-backend/}
        ]
    },
    eslint: {
        configFile: '.eslintrc'
    },
    plugins: [
        new WebpackNotifierPlugin()
    ]
};
