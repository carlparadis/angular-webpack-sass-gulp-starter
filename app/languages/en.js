/* global angular, module, status, require */
'use strict';

/**
 *  en.js is an example translation file used by Translation config / Angular-Translate
 */

var enMessages = {
    "demo" : {
        "hello": "Hello",
        "boomMe": "Boom me!"
    }
};

module.exports = enMessages;
