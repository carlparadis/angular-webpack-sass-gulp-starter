/* global angular, module, require */
'use strict';

/**
 *  MainController is considered top level, and parent to all views.
 *  It is a great place to add $stateChange listeners for routing and authentication
 */

class MainController {

    /* @ngInject */
    constructor($scope, $state, $sessionStorage) {
        this.title = "Angular Starter Loaded!";

        $scope.$on('$stateChangeStart', (event, toState, toParams) => {
            console.log('$stateChangeStart toState', toState);
            let currentRouteProtected = (toState.data && toState.data.protected ? toState.data.protected : false),
                isAuthenticated = true;  //TODO: This probably should go to an Authentication Factory/Service;
            if(currentRouteProtected){
                //TODO: Force clear session storage or cookies?
            }
            if(!isAuthenticated && currentRouteProtected){
                event.preventDefault();
                //TODO: probably should capture where the user intended to go and store it in $sessionStorage
                $sessionStorage.postAuthRoute = {
                    name: toState.name,
                    params: toParams
                };
                $state.go("demo", null, { reload: true });//TODO: This would point to a login
            }
        });

        $scope.$on('$stateChangeSuccess', (event, toState, toParams) => {
            console.log('$stateChangeSuccess toState', toState);
            let currentRouteProtected = (toState.data && toState.data.protected ? toState.data.protected : false),
                isAuthenticated = true;  //TODO: This probably should go to an Authentication Factory/Service;

            if(!isAuthenticated && currentRouteProtected){
                event.preventDefault();
            }
        });
    }

}

module.exports = MainController;
