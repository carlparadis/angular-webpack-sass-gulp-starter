/* global angular, module, require */
'use strict';

let MainModule = angular.module('app.main', []);
MainModule.controller('MainController', require('./main-controller'));

module.exports = MainModule;
