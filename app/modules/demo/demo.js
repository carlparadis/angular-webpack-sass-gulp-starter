/* global angular, module, require */
'use strict';

let DemoModule = angular.module('app.demo', [])
    .controller('DemoController', require('./demo-controller'))
    .factory('DemoFactory', require('./demo-factory'))
    .service('DemoApiService', require('./demo-api-service'))
    .config(require('./demo-routes'));

module.exports = DemoModule;
