/* global module */
'use strict';

/**
 *  DemoApiService is an example service that provides a layer for async CRUD api tasks
 */

class DemoApiService {

    /* @ngInject */
    constructor($http, uriConfig) {
        this.$http = $http;
        this.uriConfig = uriConfig;
        this.demoResults = {};
    }

    formatResponse(rawResponse) {
        return typeof rawResponse === "string" ? JSON.parse(rawResponse) : rawResponse;
    }

    getDemoResults(personId) {
        return this.$http({
            method: "GET",
            url: `${this.uriConfig.BASE_API.uri}/people/${personId}`,
            transformResponse: this.formatResponse
        }).then(response => {
            this.demoResults = response.data;
            return this.demoResults;
        }).catch(error => {
            console.error('DemoApiService getDemoResults() failed', error);
            throw error;
        });
    }

}

module.exports = DemoApiService;
