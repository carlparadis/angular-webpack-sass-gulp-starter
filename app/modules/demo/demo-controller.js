/* global angular, module, require */
'use strict';

/**
 *  DemoController is an example controller that takes data from a route resolve and manipulates it with
 *  user interaction as well as factory/service dependencies
 */

class DemoController {

    /* @ngInject */
    constructor(demoApiData, DemoFactory) {
        console.log('demoApiData', demoApiData);

        this.demoApiData = demoApiData;
        this.DemoFactory = DemoFactory;

        let babelBoom = 'Babel is booming';
        let babelTestObj = {
            babelBoom, sayBabelBoom(){
                console.log(this.babelBoom);
            }
        };

        babelTestObj.sayBabelBoom();

        this.toBlahOrNotToBlah = 'nada';
        this.beenBlahed = undefined;
    }

    boomMethod(apiData) {
        let name = this.DemoFactory.boomMe();

        console.log('booooooooom?');

        this.demoApiData.name = name;
    }

    blahMethod(whatToBlah) {
        console.log('Is ' + whatToBlah + ' blah?');

        this.beenBlahed = this.DemoFactory.blahMe(whatToBlah);
    }

}

module.exports = DemoController;
