/* global angular, describe, inject, beforeEach, it, module */
'use strict';

describe("DemoController", function () {
    // declare variables needed across methods
    var demoCtrl,
        demoApiData;

    var appDemo = require('./demo.js');

    beforeEach(angular.mock.module('ui.router'));
    beforeEach(angular.mock.module('app.demo'));

    beforeEach(angular.mock.module(function ($provide) {
        demoApiData = {
            "hello": "hello test"
        };

        $provide.value('demoApiData', demoApiData);
    }));

    beforeEach(inject(function ($controller) {
        demoCtrl = $controller('DemoController');
    }));

    // Receives backend data (mocked)
    it('Should produce a passed test', function () {
        expect(demoCtrl.demoApiData).not.toBeNull();
    });

});
