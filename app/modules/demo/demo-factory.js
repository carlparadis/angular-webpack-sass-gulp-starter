/* global module */
'use strict';

/**
 *  DemoFactory is an example factory that provides a business logic layer
 */

const DemoFactory = function () {
    //arrow function
    let blahMe = whatToBlah => `${whatToBlah} is blah...`;

    let boomMe = function () {
        return 'Boooooooooooooooom!!!!!!!';
    };

    //auto mapping/shorthand assignment for Object literals
    return {
        boomMe,
        blahMe
    };
};

module.exports = DemoFactory;
