/* global module */
'use strict';

/* @ngInject */
function demoRoutes($stateProvider) {
    $stateProvider.state("demo", {
        url: "/demo/:personId",
        templateUrl: 'demo/demo.html',
        controller: 'DemoController as demoCtrl',
        data: {
            protected: false
        },
        resolve: {
            demoApiData: ['$stateParams', 'DemoApiService', function ($stateParams, DemoApiService) {
                return DemoApiService.getDemoResults($stateParams.personId);
            }]
        }
    });
}

module.exports = demoRoutes;
