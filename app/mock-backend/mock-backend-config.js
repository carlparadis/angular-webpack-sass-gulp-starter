/* global angularMockBack, mockData */
;(function() {
"use strict";

// Routes for mock-backend to intercept from ApiService calls.  The routes here match the currently planned API routes.
var mockBackendConfig = {
	moduleName: 'app',
	mappings: [
        {
          url: /\/people/,
          method: 'GET',
          code: 200,
          body: mockData.demoMockOne
        }
	]
};

angularMockBack(mockBackendConfig);

}());
