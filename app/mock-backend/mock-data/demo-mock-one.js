/* global angularMockBack, mockData */
"use strict";
(function (mockData) {
    mockData.demoMockOne = {
        "name": "Boba Fett",
        "height": "183",
        "mass": "78.2",
        "hair_color": "black",
        "skin_color": "fair",
        "eye_color": "brown",
        "birth_year": "31.5BBY",
        "gender": "male",
        "homeworld": "http://swapi.co/api/planets/10/",
        "films": [
            "http://swapi.co/api/films/5/",
            "http://swapi.co/api/films/3/",
            "http://swapi.co/api/films/2/"
        ],
        "species": [
            "http://swapi.co/api/species/1/"
        ],
        "vehicles": [],
        "starships": [
            "http://swapi.co/api/starships/21/"
        ],
        "created": "2014-12-15T12:49:32.457000Z",
        "edited": "2014-12-20T21:17:50.349000Z",
        "url": "http://swapi.co/api/people/22/"
    };

}(window.mockData || (window.mockData = {})));
