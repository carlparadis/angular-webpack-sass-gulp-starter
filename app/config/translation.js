/* global angular, module, require */
'use strict';

/**
 *  TranslationConfig is a the config for Angular-Translate
 */

function TranslationConfig($translateProvider) {
    $translateProvider.translations('en', require('../languages/en'));

    $translateProvider.preferredLanguage('en');
}

module.exports = ['$translateProvider', TranslationConfig];