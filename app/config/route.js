/* global angular, module, require */
'use strict';

/**
 *  RouteConfig is a the config for UI-Router
 */

let RouteConfig = function ($urlRouterProvider) {
    $urlRouterProvider.when('', '/demo/22');
    $urlRouterProvider.when('/', '/demo/22');
    $urlRouterProvider.otherwise("/demo/22");
};

module.exports = ['$urlRouterProvider', RouteConfig];
