/* global angular, module, require */
'use strict';

/**
 * Configure urls used throughout the site
 *
 * NOTE: The http protocol must be included to avoid Unknown chromium error: -6 for Android.
 *
 */
module.exports = {
    'BASE_API': {
        uri: 'BASE_API',//Since externalIntercept is true, set the uri to the Key of this object
        externalIntercept: true,
        externalInterceptUri: 'http://swapi.co/api'//Only necessary if externalIntercept is true
    }
};
