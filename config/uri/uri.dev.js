/* global angular, module, require */
'use strict';

/**
 * Configure urls used throughout the site
 *
 */
module.exports = {
    'BASE_API': {
        uri: 'http://swapi.co/api',//Since externalIntercept is true, set the uri to the Key of this object
        externalIntercept: false,
        externalInterceptUri: ''//Only necessary if externalIntercept is true
    }
};
