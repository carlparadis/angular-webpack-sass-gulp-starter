/* global module */
// Vendor Bundle configuration
"use strict";

var VendorConfig = function () {
    var bowerPath = 'bower_components/';

    return {
        getVendorFileList: function (dirPath, serverId) {
            var newDirPath = dirPath || '',
                minVendorList = [
                    newDirPath + bowerPath + 'angular/angular.min.js',
                    newDirPath + bowerPath + 'angular-ui-router/release/angular-ui-router.min.js',
                    newDirPath + bowerPath + 'angular-animate/angular-animate.min.js',
                    newDirPath + bowerPath + 'angular-aria/angular-aria.min.js',
                    newDirPath + bowerPath + 'angular-messages/angular-messages.min.js',
                    newDirPath + bowerPath + 'angular-sanitize/angular-sanitize.min.js',
                    newDirPath + bowerPath + 'angular-translate/angular-translate.min.js',
                    newDirPath + bowerPath + 'ngstorage/ngStorage.min.js'
                ],
                devVendorList = [
                    newDirPath + bowerPath + 'angular/angular.js',
                    newDirPath + bowerPath + 'angular-ui-router/release/angular-ui-router.js',
                    newDirPath + bowerPath + 'angular-animate/angular-animate.js',
                    newDirPath + bowerPath + 'angular-aria/angular-aria.js',
                    newDirPath + bowerPath + 'angular-messages/angular-messages.js',
                    newDirPath + bowerPath + 'angular-sanitize/angular-sanitize.js',
                    newDirPath + bowerPath + 'angular-translate/angular-translate.js',
                    newDirPath + bowerPath + 'ngstorage/ngStorage.js'
                ],
                vendorList = minVendorList;
            if(serverId && (serverId === 'localnoapi' || serverId === 'localwithapi')){
                vendorList = devVendorList;
            }
            return vendorList;
        },
        getMockVendorFileList: function (dirPath) {
            var newDirPath = dirPath || '';

            return [
                newDirPath + bowerPath + 'angular-mocks/angular-mocks.js',
                newDirPath + bowerPath + 'angular-mock-back/angular-mock-back.js'
            ];
        }
    };
};

module.exports = VendorConfig;
