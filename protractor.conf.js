exports.config = {
  specs: ['./e2e/*_test.js'],

  capabilities: {
      'browserName': 'chrome'
  },

  // allows for easy use of ES6 through babel shimming
  // https://github.com/angular/protractor/issues/2049
  // https://babeljs.io/docs/usage/require/
  onPrepare: function () {
    require("babel/register");
  }
};
