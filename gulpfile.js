"use strict";
var argv = require("yargs").argv;
var gulp = require('gulp');
var runSequence = require('run-sequence');
var browserSync = require('browser-sync');
var request = require('request');
var webpack = require('webpack');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var globby = require('globby');
var gutil = require('gulp-util');
var KarmaServer = require('karma').Server;
var preprocess = require('gulp-preprocess');
var rename = require('gulp-rename');
var del = require('del');
var _ = require('lodash');
var angularTemplateCache = require('gulp-angular-templatecache');
var path = require('path');
var vendorConfig = require('./vendor.conf.js');
var webpackConfig = require('./webpack.conf.js');

var appBundleName = "bundle.js";
var vendorBundleName = "vendor.bundle.js";
var mockBundleName = "mockBundle.js";
var vendorMockBundleName = "vendor.mockBundle.js";

var serverId = argv.serverId || "localnoapi";
var buildType = argv.buildType || "release";

var isProd = function () {
    return (serverId === 'prod');
};

var isLocal = function () {
    return (serverId === 'localnoapi' || serverId === 'localwithapi');
};

var isDebug = function () {
    return (isLocal() || buildType === 'debug');
};

/*eslint-disable camelcase */
var prodUglifyOptions = {mangle: true, compress: {drop_console: true}};
var debugUglifyOptions = {mangle: false, compress: false, output: {beautify: true}};
var uglifyOptions = (isDebug() ? debugUglifyOptions : prodUglifyOptions);
/*eslint-enable camelcase */

var appDir = './app';
var distDir = './dist';

//FILE Arrays for copying, and watching
var SCSS_WATCH_FILES = [appDir + '/scss/app.scss', appDir + '/components/**/*.scss', appDir + '/modules/**/*.scss',
    appDir + '/scss/singletons.scss', appDir + '/scss/app-*.scss'];
var TEMPLATE_WATCH_FILES = ['!' + appDir + '/index.html', '!' + appDir + '/mock.html', '!' + appDir + '/mock-backend/**/*.html',
    appDir + '/modules/**/*.html'];
var MOCK_WATCH_FILES = [appDir + '/index.html', appDir + '/mock-backend/mock-backend.html', appDir + '/mock-backend/mock-img/**/*',
    appDir + '/mock-backend/mock-backend-config.js'];
var SCSS_BUILD_FILE = appDir + '/scss/app.scss';
var SCSS_MOBILE_BUILD_FILE = appDir + '/scss/mobile.scss';
var MOVE_ONLY_FOR_MOCK_FILES = ['!' + appDir + '/bower_components/angular-mocks/**/*',
    '!' + appDir + '/bower_components/angular-mock-back/**/*'];
var IMG_FILES = [appDir + '/img/**/*'];
var FONT_FILES = [appDir + '/bower_components/fontawesome/fonts/**/*'];
var BUNDLER_FILES = [appDir + '/app.js'];
var VENDOR_BUNDLER_FILES = vendorConfig().getVendorFileList(appDir + '/', serverId);
var MOCK_BUNDLER_FILES = [appDir + '/mock-backend/mock-data/**/*.js'];
var MOCK_VENDOR_BUNDLER_FILES = vendorConfig().getMockVendorFileList(appDir + '/');

if (argv.mobile) {
    BUNDLER_FILES.push(appDir + '/mobile.js');
    SCSS_WATCH_FILES.push(appDir + '/scss/mobile.scss');
}

var handleErrors = function (err) {
    gutil.beep();
    gutil.log(gutil.colors.bgRed('Error'), err.message);
    return this.end();
};

var buildWebpack = function (config) {
    config.plugins.push(new webpack.optimize.UglifyJsPlugin(uglifyOptions));
    webpack(config, function (err, stats) {
        if (err) {
            throw new gutil.PluginError('Webpack bundle', err);
        }
        gutil.log('[webpack]', stats.toString({
            colors: true,
            chunks: false
        }));
    });
};

var getUriConfig = function (curServerId) {
    return "./config/uri/uri." + curServerId + ".js";
};

var getSettingsConfig = function (curServerId) {
    return "./config/settings/settings." + curServerId + ".js";
};

gulp.task('config', function () {
    var uriConfig = getUriConfig(serverId),
        settingsConfig = getSettingsConfig(serverId);
    console.log("Copying config files for " + serverId);
    gulp.src(uriConfig)
        .pipe(rename('uri.js'))
        .pipe(gulp.dest(appDir + '/config/'));

    gulp.src(settingsConfig)
        .pipe(rename('settings.js'))
        .pipe(gulp.dest(appDir + '/config/'));
});

gulp.task('bundler', function () {
    console.log("Building Bundler");
    var bundlerConfig = _.assign({}, webpackConfig);
    bundlerConfig.entry = {
        app: BUNDLER_FILES
    };
    bundlerConfig.output = {
        path: distDir + '/bundle',
        filename: appBundleName
    };
    buildWebpack(bundlerConfig);
});

gulp.task('bundler-watch', function () {
    console.log("Building Bundler Watch");
    var bundlerWatchConfig = _.assign({}, webpackConfig);
    bundlerWatchConfig.entry = {
        app: BUNDLER_FILES
    };
    bundlerWatchConfig.output = {
        path: distDir + '/bundle',
        filename: appBundleName
    };
    bundlerWatchConfig.watch = true;
    bundlerWatchConfig.devtool = 'source-map';
    buildWebpack(bundlerWatchConfig);
});

var mockDataFiles = globby.sync(MOCK_BUNDLER_FILES);
gulp.task('mock-bundler', function () {
    if (argv.mockBackend) {
        console.log("Building Mock Bundler");
        var mockBundlerConfig = _.assign({}, webpackConfig);
        mockBundlerConfig.entry = {
            app: mockDataFiles
        };
        mockBundlerConfig.output = {
            path: distDir + '/mock-backend/bundle',
            filename: mockBundleName
        };
        buildWebpack(mockBundlerConfig);
    }
});

gulp.task('mock-bundler-watch', function () {
    if (argv.mockBackend) {
        console.log("Building Mock Bundler Watch");
        var mockBundlerWatchConfig = _.assign({}, webpackConfig);
        mockBundlerWatchConfig.entry = {
            app: mockDataFiles
        };
        mockBundlerWatchConfig.output = {
            path: distDir + '/mock-backend/bundle',
            filename: mockBundleName
        };
        mockBundlerWatchConfig.watch = true;
        mockBundlerWatchConfig.devtool = 'source-map';
        buildWebpack(mockBundlerWatchConfig);
    }
});

gulp.task('test', function () {
    var server = new KarmaServer({
        configFile: path.join(__dirname, '/karma.conf.js'),
        singleRun: true
    });
    server.start();
});

gulp.task('watch-tests', function () {
    var server = new KarmaServer({
        configFile: path.join(__dirname, '/karma.conf.js')
    });
    server.start();
});

// Combines all template (HTML) files of the app into a single Angular module which adds files to the angular template cache
gulp.task('templates', function () {
    console.log("Building Template Cache");
    return gulp.src(TEMPLATE_WATCH_FILES)
        .pipe(angularTemplateCache('templates.js', {
            standalone: true,
            module: 'app.templates'
        }))
        .pipe(gulp.dest(appDir + '/config'));
});

// compile the SCSS files for web
gulp.task('scss-css-web', function () {
    console.log('Building CSS from Scss Web');
    return gulp.src(SCSS_BUILD_FILE)
        .pipe(sourcemaps.init())
        .pipe(sass({
            sync: true,
            outputStyle: (isLocal() ? 'nested' : 'compressed')
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDir + '/css'));
});

// compile the SCSS files for mobile
gulp.task('scss-css-mobile', function () {
    console.log('Building Mobile CSS from Scss');

    return gulp.src(SCSS_MOBILE_BUILD_FILE)
        .pipe(sourcemaps.init())
        .pipe(sass({
            sync: true,
            outputStyle: (isLocal() ? 'nested' : 'compressed')
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDir + '/css'));
});

// compile the SCSS files
gulp.task('scss-css', function (done) {
    console.log('Building CSS from Scss');
    var scssBuildTasks = ['scss-css-web'];

    if (argv.mobile) {
        scssBuildTasks.push('scss-css-mobile');
    }

    runSequence(scssBuildTasks, done);

});

gulp.task('mock-backend-html', function () {
    if (argv.mockBackend) {
        console.log("Creating mockBackend html");
        return gulp.src(appDir + '/index.html')
            .pipe(preprocess({
                context: {
                    mockBackend: argv.mockBackend,
                    cacheBreakParam: (argv.noCacheBreak ? "" : "?d=" + Date.now()),
                    mobile: argv.mobile
                }
            }))
            .pipe(rename('mock.html'))
            .pipe(gulp.dest(appDir));
    }
});

// Rebuild the template cache when any HTML file changes
gulp.task('watch-templates', function () {
    console.log("Watching Templates");
    gulp.watch(TEMPLATE_WATCH_FILES, ['templates']);
});

gulp.task('watch-scss', function () {
    console.log("Watching Scss");
    gulp.watch(SCSS_WATCH_FILES, ['scss-css']);
});

gulp.task('watch-images', function () {
    console.log("Watching Images");
    gulp.watch(IMG_FILES, ['copy-img']);
});

gulp.task('watch-index', function () {
    console.log("Watching Index");
    gulp.watch(appDir + '/index.html', ['copy-index']);
});

gulp.task('watch-mock-back', function () {
    console.log("Watching Mock Back");
    gulp.watch(MOCK_WATCH_FILES, ['copy-mock-back']);
});

gulp.task('copy-watch-resources', ['copy-resources'], function (done) {
    console.log("Watching and Copying Resources");
    runSequence(['watch-templates', 'watch-scss', 'watch-images', 'watch-index', 'watch-mock-back'], done);
});

gulp.task('copy-vendor-scripts', ['copy-concat-vendor', 'copy-concat-mock-vendor']);

gulp.task('copy-concat-vendor', function () {
    gulp.src(VENDOR_BUNDLER_FILES)
        .pipe(sourcemaps.init())
        .pipe(concat(vendorBundleName, {}))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(distDir + '/bundle'));
});

gulp.task('copy-concat-mock-vendor', function () {
    if (argv.mockBackend) {
        gulp.src(MOCK_VENDOR_BUNDLER_FILES)
            .pipe(sourcemaps.init())
            .pipe(concat(vendorMockBundleName, {}))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(distDir + '/mock-backend/bundle'));
    }
});

gulp.task('copy-resources', ['copy-fonts', 'copy-mock-back', 'copy-index', 'copy-img', 'copy-vendor-scripts']);

gulp.task('copy-index', function () {
    if (argv.mockBackend && argv.mobile) {
        console.log("Copying Index is not necessary when mockBackend and mobile args are true");
    } else {
        gulp.src(appDir + '/index.html')
            .pipe(preprocess({
                context: {
                    cacheBreakParam: (argv.noCacheBreak ? "" : "?d=" + Date.now()),
                    mobile: argv.mobile
                }
            }))
            .pipe(gulp.dest(distDir));
    }
});

gulp.task('copy-img', function () {
    console.log("Copying Images");
    gulp.src(IMG_FILES)
        .pipe(gulp.dest(distDir + '/img'));
});

gulp.task('copy-fonts', function () {
    console.log("Copying Font Files");

    gulp.src(FONT_FILES)
        .on('error', handleErrors)
        .pipe(gulp.dest(distDir + '/fonts'));
});

gulp.task('copy-mock-back', ['mock-backend-html'], function () {
    console.log("Copying Mock Back Files");
    if (argv.mockBackend) {
        if (argv.mobile) {
            del([distDir + '/mock.html'],
                function (err, deletedFiles) {
                    console.log('Files deleted because running mockBackend for mobile:', deletedFiles.join(', '));
                });
            gulp.src(appDir + '/mock.html')
                .pipe(rename('index.html'))
                .pipe(gulp.dest(distDir));
        } else {
            gulp.src([appDir + '/mock.html'])
                .pipe(gulp.dest(distDir));
        }
        gulp.src([appDir + '/mock-backend/mock-backend-config.js'])
            .pipe(gulp.dest(distDir + '/mock-backend'));
        gulp.src([appDir + '/mock-backend/mock-img/**/*'])
            .on('error', handleErrors)
            .pipe(gulp.dest(distDir + '/mock-backend/mock-img/'));
    } else {
        del([distDir + '/mock-backend',
                distDir + '/bower_components/angular-mock*',
                distDir + '/mock.html'],
            function (err, deletedFiles) {
                console.log('Files deleted because running without mockBackend:', deletedFiles.join(', '));
            });
    }
});

gulp.task('browser-sync', function () {
    console.log("Loading browserSync");
    var uriConfigLocation = getUriConfig(serverId), uriConfig = require(uriConfigLocation),
        uriInterceptKeyArray = [], externalIntercept = false;

    //Building an array keys for URIs that browserSync is supposed to intercept for CORS
    for (var key in uriConfig) {
        // check also if property is not inherited from prototype
        if (uriConfig.hasOwnProperty(key) && uriConfig[key].externalIntercept) {
            uriInterceptKeyArray.push(key);
        }
    }

    if (uriInterceptKeyArray.length > 0) {
        externalIntercept = true;
        console.log('browserSync will intercept the following external uriConfig keys', uriInterceptKeyArray);
    }

    var middlewareIntercept = function (req, res, next) {
        if (externalIntercept) {
            var currentUriInterceptObj, currentUriInterceptKey = '';

            // Looping through the uriInterceptKeyArray, to see if any of the keys exist in the URL browserSync is serving up.
            for (var i = 0; i < uriInterceptKeyArray.length; i++) {
                currentUriInterceptObj = undefined;
                if (_.contains(req.url, uriInterceptKeyArray[i])) {
                    // If the req.url contains a key in the uriInterceptKeyArray, the currentUriInterceptObj is populated.
                    currentUriInterceptKey = uriInterceptKeyArray[i];
                    currentUriInterceptObj = uriConfig[currentUriInterceptKey];
                    break;
                }
            }

            if (currentUriInterceptObj) {
                // If currentUriInterceptObj, then we have to parse it and intercept the URL browserSync is requesting,
                // and pipe the request to the actual external API server, to avoid CORS issues.
                var originalRequestUrl = req.url || '',
                    externalUrl = currentUriInterceptObj.externalInterceptUri + originalRequestUrl.replace('/' + currentUriInterceptKey, ''),
                    remote = request(externalUrl);
                console.log('Intercepting for CORS header for ' + req.method + ': ' + req.url, currentUriInterceptObj, externalUrl);
                res.setHeader('Access-Control-Allow-Origin', currentUriInterceptObj.externalInterceptUri);
                res.setHeader('Access-Control-Allow-Headers', 'Authorization, Auth,Token, Access-Token,Access_Token, AccessToken, Code, Content-Type');
                req.pipe(remote);
                remote.pipe(res);
            } else {
                next();
            }
        } else {
            next();
        }
    };

    browserSync({
        server: {
            baseDir: [distDir],
            middleware: [middlewareIntercept]
        },
        files: [distDir + '/**'],
        port: 9000, // keep consistent with express
        open: false // stop the browser from opening automatically
    }, function (err) {
        if (err) {
            console.log("BrowserSync Error");
            handleErrors(err);
        } else {
            console.log("BrowserSync is ready!");
        }
    });
});

gulp.task('clean', function () {
    console.log("Cleaning");
    del([distDir, appDir + '/config/uri.js', appDir + '/config/settings.js', appDir + '/config/templates.js', appDir + '/mock.html'],
        function (err, deletedFiles) {
            console.log('Files deleted because running with clean:', deletedFiles.join(', '));
        });
});

// Main build task. Runs all build-related tasks
gulp.task('build', function (done) {
    // Run Sequence runs tasks before the array synchronously, then runs the tasks in the array asynchronously, then runs tasks after the array synchronously
    runSequence('config', ['scss-css', 'templates'], 'bundler', 'mock-bundler', 'copy-resources',
        done);
});

// TDD build task. Runs all watch-related tasks.
gulp.task('default', function (done) {
    // Run Sequence runs tasks before the array synchronously, then runs the tasks in the array asynchronously, then runs tasks after the array synchronously
    runSequence('config', ['scss-css', 'templates'], 'bundler-watch', 'mock-bundler-watch', 'watch-tests',
        'copy-watch-resources', 'browser-sync',
        done);
});
