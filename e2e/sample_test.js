describe('DemoPage', () => {

    const demoPage = require('./pages/demo')();

    beforeEach(() => {
        demoPage.get();
    });

    it('should boom and blah!', () => {
        expect(demoPage.getBoomText()).toEqual('Hello Boba Fett! Boom me!');
        demoPage.boom();
        expect(demoPage.getBoomText()).toEqual('Hello Boooooooooooooooom!!!!!!!! Boom me!');

        demoPage.blah('es6');
        expect(demoPage.getBlahText()).toEqual('es6 is blah...');
    });

});
