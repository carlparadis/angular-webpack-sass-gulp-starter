const _ = require('lodash');
const pxt = require('../protractor-extensions');

const demoPage = () => {
    const el = {
        boomText: element(by.binding('demoCtrl.demoApiData.name')),
        boomButton: $('.boom button'),
        blahInput: element(by.model('demoCtrl.toBlahOrNotToBlah')),
        blahButton: $('.blah button'),
        blahText: element(by.binding('demoCtrl.beenBlahed'))
    };

    return {
        get: () => browser.get('http://localhost:9000'),
        getBoomText: el.boomText.getText,
        boom: el.boomButton.click,
        blah: _.flow(pxt.enterText(el.blahInput), el.blahButton.click),
        getBlahText: el.blahText.getText
    };
}

module.exports = demoPage;
