const _ = require('lodash');

// This is the start of an idea for some extension methods to protractor
// Once I get this in order this would be an npm module and NOT in the starter source code.
module.exports = {
    enterText: _.curry((el, text) => el.clear().sendKeys(text)),
};
